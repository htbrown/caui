# caui

> ⚠️ caui is not in a state that should be used in production environments. Please expect bugs and breaking changes.

caui (pronounced cowie) is an experimental web-based user interface for [Caddy](https://caddyserver.com) servers, intended for use with reverse proxies.